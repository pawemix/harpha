
# ToDos

* [X] drawer
* [ ] suggestions - write *encountered* date to a song file
    1. [ ] based on mtime
    2. [ ] based on a date found in the filename
* [o] list songs
    * [X] title
    * [X] author
    * [X] album
    * [X] thumbnail
    * [ ] timestamp ("User *encountered* at")
* [ ] list playlists
* [ ] select many suggestions to apply at once
* [ ] select many songs
* [ ] for selected songs, remove duplicates
    * [ ] keep the one with the oldest timestamp ("original")
    * [ ] write timestamps of all of them to some tag of the kept one
    * [ ] replace all occurences (in playlists) of removed duplicates with the original
    * [ ] index those replacements to some app database etc.
        * [ ] so that if new playlists referencing old files appear, they can be updated
        * [ ] for potential undo
* [ ] for selected songs, create a playlist
* [ ] select many playlists
* [ ] for selected playlists, create a playlist
    * [ ] either an AND or an OR union
    * [ ] either remove duplicate songs or keep with order preserved
    * [ ] sort either by playlist order or by timestamp or other tags...
* [ ] create a playlist out of all songs of an artist
    * [ ] scan also for songs with multiple artists
        * [ ] parse arist-related tags
        * [ ] split by formal separators (e.g. `&`, `/`, `;`)
        * [ ] split by popular informal separators (e.g. `feat.`, `&`, `+`, `with`, `and`, `vs.`)
* [ ] suggestions tab
    * [ ] duplicate songs
        * [ ] based on file name
        * [ ] based on song hash/fingerprint (<https://musicbrainz.org/doc/Fingerprinting>)
        * [ ] based on file size
        * [ ] based on song length
        * [ ] based on song length & song tags
        * [ ] the more conditions apply, the more likely it is that the songs are duplicates
        * [ ] suggest above certain probability treshold
        * [ ] sort them by probability
        * [ ] show first 10
    * [ ] tags to correct
        * [ ] prettify chaotic tags
            * [ ] e.g. title case
        * [ ] normalize conventions of various tags
            * [ ] e.g. separators of multiple artists
    * [ ] manually scan for suggestions (explicit button click)
    * [ ] automatically scan for suggestions (background service)
* [ ] flows/automation tab
    * [ ] read Favorites playlist from Metro
        1. [ ] dump it into the app with timestamps
        2. [ ] clear Favorites playlist in Metro
        3. [ ] create a playlist in the app with a timestamp (period, e.g. day, week, month)
        4. [ ] different scopes, e.g. day & week & month
        5. [ ] scopes intersect, e.g. month contains weeks' & days' averaged most frequent
    * [ ] adding song files newly added to the filesystem (e.g. downloaded) to a (e.g. Encounters) playlist
