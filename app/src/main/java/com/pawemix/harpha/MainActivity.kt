@file:OptIn(ExperimentalMaterial3Api::class)

package com.pawemix.harpha

import android.media.MediaMetadataRetriever
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.*
import androidx.compose.material3.*
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.pawemix.harpha.domain.Song
import com.pawemix.harpha.domain.M3UPlaylist
import com.pawemix.harpha.domain.map
import com.pawemix.harpha.domain.Either
import com.pawemix.harpha.io.scanForSongs
import com.pawemix.harpha.io.scanPlaylists
import com.pawemix.harpha.ui.AutomationsScreen
import com.pawemix.harpha.ui.DownloadsScreen
import com.pawemix.harpha.ui.PlaylistsScreen
import com.pawemix.harpha.ui.SongsScreen
import com.pawemix.harpha.ui.SuggestionsScreen
import com.pawemix.harpha.ui.PlaylistView
import com.pawemix.harpha.ui.util.Loadable
import java.io.File
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

// TODO: check if READ_EXTERNAL_STORAGE permission granted; if not - request it
// TODO: if too many songs, paginate it / load lazily
// TODO: even if list of songs the same after a refresh, inform after refresh
// (separate state of refreshing and state of the songs list)

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent { MainApp() }
    }
}


enum class Tab {
    Playlists,
    Songs,
    Suggestions,
    Automations,
    Downloads,
}


@Composable
private fun MainApp() = MaterialTheme {
    val snackbarHostState = remember { SnackbarHostState() }
    val coroutineScope = rememberCoroutineScope()
    val drawerState = rememberDrawerState(DrawerValue.Closed)

    val (songs, setSongs) =
            remember { mutableStateOf<List<Song>?>(null) }
    val (playlists, setPlaylists) =
            remember { mutableStateOf<Loadable<Throwable, List<PlaylistView>>>(Loadable.Idle) }

    suspend fun refreshSongsScreen() {
        coroutineScope.launch(Dispatchers.Main) {
            snackbarHostState.showSnackbar(
                message = "Scanning for songs...",
                withDismissAction = true,
                duration = SnackbarDuration.Short,
            )
        }
        coroutineScope.launch(Dispatchers.IO) { scanForSongs(setSongs) }
    }

    suspend fun refreshPlaylistsScreen() {
        coroutineScope.launch(Dispatchers.Main) {
            setPlaylists(Loadable.InProgress)
            snackbarHostState.showSnackbar(
                message = "Scanning for playlists...",
                withDismissAction = true,
                duration = SnackbarDuration.Short,
            )
        }
        coroutineScope.launch(Dispatchers.IO) {
            val scannedPlaylists = scanPlaylists()
            when (scannedPlaylists) {
                is Either.Left -> {
                    setPlaylists(Loadable.Failure(scannedPlaylists.left))
                }
                is Either.Right -> {
                    val plists: Sequence<M3UPlaylist> = scannedPlaylists.right
                    val plists_: Sequence<PlaylistView> = plists.map {
                        PlaylistView(it.absolutePath)
                    }
                    val plists__: Sequence<List<PlaylistView>> =
                            plists_.scan(emptyList()) { acc, p -> acc + p }
                    plists__.forEach { setPlaylists(Loadable.Success(it)) }
                }
            }
        }
    }

    val (tab, setTab) = remember { mutableStateOf(Tab.Songs) }
    val (refresh, setRefresh) = remember {
        mutableStateOf<suspend () -> Unit>({ refreshSongsScreen() })
    }

    Scaffold(
            snackbarHost = { SnackbarHost(snackbarHostState) },
            modifier = Modifier.fillMaxSize(),
            topBar = {
                TopAppBar(
                        title = { Text("Harpha") },
                        navigationIcon = {
                            IconButton(
                                    onClick = {
                                        coroutineScope.launch {
                                            drawerState.open()
                                        }
                                    }
                            ) { Icon(Icons.Rounded.Menu, contentDescription = "Menu") }
                        },
                        actions = {
                            IconButton(onClick = {}) {
                                Icon(Icons.Rounded.Settings, contentDescription = "Settings")
                            }
                        }
                )
            },
            floatingActionButton = {
                FloatingActionButton(
                        onClick = { coroutineScope.launch { refresh.invoke() } },
                ) { Icon(Icons.Rounded.Refresh, contentDescription = "Refresh list") }
            }
    ) { paddingValues ->
        ModalNavigationDrawer(
            gesturesEnabled = drawerState.isOpen,
            drawerState = drawerState,
            drawerContent = {
                ModalDrawerSheet {
                    // TODO: dynamically retrieve top app bar height:
                    Spacer(modifier = Modifier.height(64.dp))
                    NavigationDrawerItem(
                        label = { Text("Songs") },
                        icon = { Icon(Icons.Rounded.MusicNote, "Songs") },
                        selected = tab == Tab.Songs,
                        onClick = {
                            setTab(Tab.Songs)
                            setRefresh({ refreshSongsScreen() })
                            coroutineScope.launch { drawerState.close() }
                        }
                    )
                    NavigationDrawerItem(
                        label = { Text("Playlists") },
                        icon = { Icon(Icons.Rounded.PlaylistPlay, "Playlists") },
                        selected = tab == Tab.Playlists,
                        onClick = {
                            setTab(Tab.Playlists)
                            setRefresh({ refreshPlaylistsScreen() })
                            coroutineScope.launch { drawerState.close() }
                        }
                    )
                    NavigationDrawerItem(
                        label = { Text("Suggestions") },
                        icon = { Icon(Icons.Rounded.Lightbulb, "Suggestions") },
                        selected = tab == Tab.Suggestions,
                        onClick = {
                            setTab(Tab.Suggestions)
                            setRefresh({})
                            coroutineScope.launch { drawerState.close() }
                        }
                    )
                    NavigationDrawerItem(
                        label = { Text("Automations") },
                        icon = { Icon(Icons.Rounded.Bolt, "Automations") },
                        selected = tab == Tab.Automations,
                        onClick = {
                            setTab(Tab.Automations)
                            setRefresh({})
                            coroutineScope.launch { drawerState.close() }
                        }
                    )
                    NavigationDrawerItem(
                        label = { Text("Downloads") },
                        icon = { Icon(Icons.Rounded.Download, "Downloads") },
                        selected = tab == Tab.Downloads,
                        onClick = {
                            setTab(Tab.Downloads)
                            setRefresh({})
                            coroutineScope.launch { drawerState.close() }
                        }
                    )
                }
            },
        ) {
            Box(
                modifier = Modifier.padding(paddingValues).fillMaxSize(),
            ) {
                when (tab) {
                    Tab.Songs -> SongsScreen(
                        songs,
                        paddingValues,
                        false,
                        snackbarHostState,
                        coroutineScope,
                    )
                    Tab.Playlists -> PlaylistsScreen(playlists)
                    Tab.Suggestions -> SuggestionsScreen()
                    Tab.Automations -> AutomationsScreen()
                    Tab.Downloads -> DownloadsScreen()
                }
            }
        }
    }
}

