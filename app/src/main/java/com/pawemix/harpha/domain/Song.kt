package com.pawemix.harpha.domain


import androidx.compose.ui.graphics.ImageBitmap


data class Song(
        val title: String,
        val artist: String? = null,
        val album: String? = null,
        val picture: ImageBitmap? = null,
        val filePath: String,
        val encounteredDate: Long? = null,
        val modifiedDate: Long,
)

