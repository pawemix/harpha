package com.pawemix.harpha.domain

sealed class Either<L, R> {
    data class Left<L, R>(val left: L) : Either<L, R>()
    data class Right<L, R>(val right: R) : Either<L, R>()
}

fun <L, R, RO> Either<L, R>.map(f: (R) -> RO): Either<L, RO> = when (this) {
    is Either.Left -> Either.Left(left)
    is Either.Right -> Either.Right(f(this.right))
}

fun <L, R> Either<L, R>.swap(): Either<R, L> = when (this) {
    is Either.Left -> Either.Right(left)
    is Either.Right -> Either.Left(right)
}

fun <L, R> Either<L, R>.default(default: R): R = when (this) {
    is Either.Left -> default
    is Either.Right -> right
}

