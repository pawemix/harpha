package com.pawemix.harpha.io

import com.pawemix.harpha.domain.M3UPlaylist
import com.pawemix.harpha.domain.Either
import java.io.File
import android.util.Log

fun scanPlaylists(): Either<Throwable, Sequence<M3UPlaylist>> {
    val rootDir = File("/storage/emulated/0/Music")
    if (!rootDir.isDirectory || !rootDir.canRead()) {
        return Either.Left(IllegalStateException("Cannot read from $rootDir"))
    }
    return Either.Right(
            rootDir.walkTopDown()
                .filter { it.extension == "m3u" }
                .map { M3UPlaylist(it.absolutePath) }
    )
}
