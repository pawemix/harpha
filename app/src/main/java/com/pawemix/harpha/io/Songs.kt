package com.pawemix.harpha.io

import com.pawemix.harpha.domain.Song
import java.io.File
import android.media.MediaMetadataRetriever
import android.graphics.BitmapFactory
import android.util.Log
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asImageBitmap

// TODO:
// https://developer.android.com/reference/androidx/media3/extractor/metadata/id3/package-summary

suspend fun scanForSongs(setSongs: (List<Song>?) -> Unit): Unit {
    val musicDir = File("/storage/emulated/0/Music/pub/SpotiFlyer")
    val getMetadata = MediaMetadataRetriever()
    val loadedSongs =
            musicDir.walkBottomUp()
                    .filter {
                        it.isFile &&
                                it.extension in listOf("mp3", "flac", "ogg", "wav", "opus") &&
                                it.canRead()
                    }
                    .asSequence()
                    .map { file ->
                        try {
                            getMetadata.setDataSource(file.absolutePath)
                            Song(
                                    title =
                                            getMetadata.extractMetadata(
                                                    MediaMetadataRetriever.METADATA_KEY_TITLE
                                            )
                                                    ?: file.nameWithoutExtension,
                                    artist =
                                            getMetadata.extractMetadata(
                                                    MediaMetadataRetriever.METADATA_KEY_ARTIST
                                            ),
                                    album =
                                            getMetadata.extractMetadata(
                                                    MediaMetadataRetriever.METADATA_KEY_ALBUM
                                            ),
                                    // picture = null,
                                    picture = retrievePrimaryImage(getMetadata),
                                    filePath = file.path,
                                    modifiedDate = file.lastModified(),
                                    encounteredDate = getMetadata.extractMetadata(
                                            MediaMetadataRetriever.METADATA_KEY_DATE,
                                    )?.toLongOrNull(),
                            )
                        } catch (e: Exception) {
                            null
                        }
                    }
                    .filterNotNull()
                    .filter { it.picture != null }
                    .scan(emptyList<Song>()) { acc, song -> acc + song }
                    .take(50)
                    .forEach { setSongs(it) }
    getMetadata.release()
}


fun retrievePrimaryImage(metadata: MediaMetadataRetriever): ImageBitmap? {
    try {
        val bmp =
            metadata.getEmbeddedPicture()
                ?.let { BitmapFactory.decodeByteArray(it, 0, it.size)?.asImageBitmap() }
            ?: metadata.getPrimaryImage()?.asImageBitmap()
        return bmp
    } catch (e: Exception) {
        Log.d("Harpha", e.toString())
        return null
    }
}
