package com.pawemix.harpha.ui

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.pawemix.harpha.ui.util.Loadable

data class PlaylistView(
    val name: String,
    val thumbnail: ImageBitmap? = null,
)

@Composable
fun PlaylistsScreen(playlists: Loadable<Throwable, List<PlaylistView>>) =
    when (playlists) {
        is Loadable.Success -> {
            val plists = playlists.result
            LazyColumn {
                items(plists.size) { idx -> PlaylistCard(plists[idx]) }
            }
        }
        is Loadable.Failure -> {
            Text(
                text = "Error ${playlists.error}",
                modifier = Modifier.fillMaxWidth().padding(8.dp),
                textAlign = TextAlign.Center,
            )
        }
        is Loadable.Idle -> {
            Text(
                text = "Press refresh button to load playlists",
                modifier = Modifier.fillMaxWidth().padding(8.dp),
                textAlign = TextAlign.Center,
            )
        }
        is Loadable.InProgress -> {
            Text(
                text = "Playlists loading...",
                modifier = Modifier.fillMaxWidth().padding(8.dp),
                textAlign = TextAlign.Center,
            )
        }
    }

@Composable
private fun PlaylistCard(playlist: PlaylistView) {
    Column(
        modifier = Modifier.fillMaxWidth().padding(8.dp),
    ) {
        Text(
            text = playlist.name,
            textAlign = TextAlign.Start,
        )
    }
}

