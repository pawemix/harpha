package com.pawemix.harpha.ui


import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.foundation.layout.*
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import com.pawemix.harpha.domain.Song
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch


@Composable
fun SongsScreen(
    songs: List<Song>?,
    paddingValues: PaddingValues,
    finishedLoading: Boolean = false,
    snackbarHostState: SnackbarHostState,
    coroutineScope: CoroutineScope,
) {
    Column() {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.background,
        ) {
            songs?.let { songs ->
                LazyColumn(
                    modifier = Modifier.fillMaxSize(),
                    verticalArrangement = Arrangement.spacedBy(8.dp),
                ) {
                    items(songs.size) { idx -> SongCard(songs[idx]) }
                }
            } ?: Text("No songs yet.")
            LaunchedEffect(finishedLoading) {
                if (finishedLoading && songs != null) {
                    coroutineScope.launch {
                        snackbarHostState.showSnackbar(
                            message = "Found ${songs.size} songs",
                            duration = SnackbarDuration.Short,
                        )
                    }
                }
            }
        }
    }
}


@Composable
private fun SongCard(song: Song) {
    Card(modifier = Modifier.fillMaxWidth().height(64.dp)) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.spacedBy(8.dp),
        ) {
            song.picture?.let {
                Image(
                    it,
                    "Album cover",
                    Modifier.size(48.dp),
                    contentScale = ContentScale.Fit,
                )
            }
            Column(verticalArrangement = Arrangement.SpaceBetween) {
                Text(song.title)
                Text(song.artist ?: "Unknown artist")
            }
        }
    }
}
