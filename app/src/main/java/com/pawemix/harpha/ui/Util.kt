package com.pawemix.harpha.ui.util

sealed class Loadable<out E, out R> {
    object Idle : Loadable<Nothing, Nothing>()
    object InProgress : Loadable<Nothing, Nothing>()
    data class Success<R>(val result: R) : Loadable<Nothing, R>()
    data class Failure<E>(val error: E) : Loadable<E, Nothing>()
}
